<?php

require 'database.php';
require 'Transactions.php';

$transaction_id = $_GET['payment_reference'];
$message = json_decode($_GET['message'], JSON_FORCE_OBJECT);
$error_message = $_GET['message'];

switch ($message['payment_status_code']) {
    case 'PSC_1':
        $status = 101;
        break;
    case 'PSC_2':
        $status = 300;
        break;
    case 'PSC_3':
        $status = 400;
        break;
    default:
        $status = 0;
}

$transactions = new Transactions($db);

$notification = $message['payment_status_label'];

if (count($message) > 1) {
    foreach ($message as $value => $key) {
        if ($value != 'payment_status_label' && $value != 'payment_status_code') {
            $additional = ucfirst($key);
        }
    }
}
if (!isset($message['payment_status_code'])) {
    $additional = $error_message;
}

$check = $transactions->transactionExists($transaction_id);

if ($check == true) {
    $transactions->transactionUpdate($transaction_id, $status, $notification, $additional);
} else {
    $transactions->transactionsStore($transaction_id, $status, $notification, $additional);
}



