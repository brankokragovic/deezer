<?php
try {

    $db = new PDO("mysql:host=database1;dbname=deezer", "homestead", "secret");

    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $err) {
    echo "ERROR: Unable to connect: " . $err->getMessage();
}
