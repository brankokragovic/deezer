DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions`
(
  `transaction_id` VARCHAR(45) NOT NULL,
  `status`         ENUM('0', '101', '300', '400') NOT NULL DEFAULT '0',
  `notification`   VARCHAR(45) DEFAULT NULL,
  `additional`     VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
)
  engine=innodb
  DEFAULT charset=utf8;


INSERT INTO transactions
            (transaction_id)
VALUES      ('a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'),
            ('0b9c2625dc21ef05f6ad4ddf47c5f203837aa32c'),
            ('1d95f084290bfb40eeb03b9e831b44bff1de87a1'),
            ('839fa1b15ebcb8f8d6e1eddf975af4b1254db1f0'),
            ('f57b82ae7be315ebb9b9eb23541a3a8ffb767357'),
            ('71f2cb5f9399d78b5591d2716a601e7dcc8bd741'),
            ('21ac1eef293eedfb31ce79eafa5b577997068516');

