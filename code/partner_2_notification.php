<?php

require 'database.php';
require 'Transactions.php';

$transaction_id = $_GET['merchant_free_field']['transaction_id'];
$message = $_GET['status'];

switch ($message) {
    case '1':
        $status = 101;
        $notification = 'Process started';
        break;
    case '2':
        $status = 300;
        $notification = 'Payment succeeded';
        break;
    case '3':
        $status = 400;
        $notification = 'Payment failed';
        break;
    default:
        $status = 0;
}

$transactions = new Transactions($db);
$additional = '';

foreach ($_GET['merchant_free_field'] as $value => $key) {
    if ($value != 'transaction_id') {
        $additional = $key;
    }
}

$check = $transactions->transactionExists($transaction_id);

if ($check == true) {
    $transactions->transactionUpdate($transaction_id, $status, $notification, $additional); //put additional here
} else {
    $transactions->transactionsStore($transaction_id, $status, $notification, $additional); //put additional here
}

