<?php

$options = getopt('', ['host:', 'verbose']);

if (!isset($options['host'])) {

	echo 'Usage:' . PHP_EOL
	, 'Options:' . PHP_EOL
	, '  --host    The host for calling partner_*_notification.php.' . PHP_EOL
	, '  --verbose Activate the verbose mode.' . PHP_EOL;

	exit(1);
}

$host = $options['host'];
$isVerboseModeOn = isset($options['verbose']);

$partnersNotificationsParameters = [
	'partner_1' => [
		[
			'payment_reference' => 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',
			'message' => '{"payment_status_code":"PSC_1","payment_status_label":"Payment process started"}'
		],
		[
			'payment_reference' => '0b9c2625dc21ef05f6ad4ddf47c5f203837aa32c',
			'message' => '{"payment_status_code":"PSC_1","payment_status_label":"Payment process started"}'
		],
		[
			'payment_reference' => 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',
			'message' => '{"payment_status_code":"PSC_2","payment_status_label":"Payment process succeeded"}'
		],
		[
			'payment_reference' => '0b9c2625dc21ef05f6ad4ddf47c5f203837aa32c',
			'message' => '{"payment_status_code":"PSC_3","payment_status_label":"Payment process failed","reason":"expired credit card"}'
		],
		[
			'payment_reference' => '1d95f084290bfb40eeb03b9e831b44bff1de87a1',
			'message' => '{"payment_status_code":"PSC_2","payment_status_label":"Payment process succeeded"}'
		],
		[
			'payment_reference' => '839fa1b15ebcb8f8d6e1eddf975af4b1254db1f0',
			'message' => '{"payment_status_code":"PSC_4","payment_status_label":"It\'s a trap"}'
		],
		[
			'payment_reference' => 'f57b82ae7be315ebb9b9eb23541a3a8ffb767357',
			'message' => 'java.io.IOException: Error writing to server.'
		]
	],
	'partner_2' => [
		[
			'merchant_free_field[transaction_id]' => '71f2cb5f9399d78b5591d2716a601e7dcc8bd741',
			'status' => 1
		],
		[
			'merchant_free_field[transaction_id]' => '71f2cb5f9399d78b5591d2716a601e7dcc8bd741',
			'status' => 2
		],
		[
			'merchant_free_field[transaction_id]' => '21ac1eef293eedfb31ce79eafa5b577997068516',
			'merchant_free_field[foo]' => 'bar',
			'status' => 1
		],
	]
];

$curlSession = curl_init();

foreach ($partnersNotificationsParameters as $partnerName => $partnerNotificationsParameters) {

	$endpoint = $host . '/' . $partnerName . '_notification.php';

	foreach ($partnerNotificationsParameters as $partnerNotificationParameters) {

		$notification = $endpoint . '?' . http_build_query($partnerNotificationParameters);

		if (!$isVerboseModeOn) {
			curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
		}

		curl_setopt($curlSession, CURLOPT_URL, $notification);
		curl_exec($curlSession);
		$lastCallInfo = curl_getinfo($curlSession);

		echo ($lastCallInfo['http_code'] === 200 ? 'success' : 'failure'), ':', $notification, PHP_EOL;
	}
}

curl_close($curlSession);
