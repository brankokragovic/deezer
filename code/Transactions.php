<?php

require 'database.php';

class Transactions
{
    private $db;

    public function __construct($database)
    {
        $this->db = $database;
    }

    public function getStatus($transaction_id)
    {
        $query = $this->db->prepare('SELECT status  FROM transactions WHERE transaction_id = ?');
        $query->bindValue(1, $transaction_id);

        try {
            $query->execute();
            return $query->fetch();
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function transactionExists($transaction_id)
    {
        $query = $this->db->prepare("SELECT COUNT(transaction_id) FROM transactions WHERE transaction_id= ?");
        $query->bindValue(1, $transaction_id);

        try {
            $query->execute();
            $rows = $query->fetchColumn();

            if ($rows == 1) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function transactionsStore($transaction_id, $status, $notification, $additional)
    {
        $query = $this->db->prepare("INSERT INTO transactions ( transaction_id ,status ,notification ,additional) VALUES (?, ? ,? ,?) ");
        $query->bindValue(1, $transaction_id);
        $query->bindValue(2, $status);
        $query->bindValue(3, $notification);
        $query->bindValue(4, $additional);

        try {
            $query->execute();
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function transactionUpdate($transaction_id, $status, $notification, $additional)
    {
        $query = $this->db->prepare("UPDATE transactions  SET status = ? ,notification = ? ,additional = ? WHERE transaction_id = ?");
        $query->bindValue(1, $status);
        $query->bindValue(2, $notification);
        $query->bindValue(3, $additional);
        $query->bindValue(4, $transaction_id);

        try {
            $query->execute();
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
